﻿using System.Configuration;

namespace WeatherReport.Classes
{
    public class Keys
    {
        public static string OWMapi
        {
            get
            {
                return ConfigurationSettings.AppSettings["OWMapi"];
            }
        }

        public static string WeatherAppId
        {
            get
            {
                return ConfigurationSettings.AppSettings["WeatherAppId"];
            }
        }
    }
}