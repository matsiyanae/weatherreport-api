﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using WeatherReport.Models;
using WeatherReport.WeatherServices;

namespace WeatherReport.Controllers
{
    //CORS for dev purposes // Open access here for final URL
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WeatherController : ApiController
    {
        // POST: api/weatherreport
        /*
         * Two city names
         * Post to Open Weather Map
         * Create a cleaner object for the from end
         * Return the current weather status for the two cities
        */
        [Route("api/weatherreport")]
        [HttpPost]
        [ResponseType(typeof(ResponseData))]
        public async Task<ResponseData> weatherReport([FromBody] RequestTwoCities cities)
        {
            try
            {
                //Should validate the two city names before trying to use them
                if (String.IsNullOrEmpty(cities.cityone) || String.IsNullOrEmpty(cities.citytwo)) {
                    return new ResponseData() { Status = 400, Data = "", Message = "Please provide both City names." };
                }

                CityWeather cityWeather = new CityWeather();
                string cityOne = await CityWeather.getCurrentWeather(cities.cityone);
                string cityTwo = await CityWeather.getCurrentWeather(cities.citytwo);

                if (cityOne.Equals("404") || cityTwo.Equals("404")) {
                    return new ResponseData() { Status = 400, Data = "", Message = "Please provide both City names." };
                }

                //City One
                dynamic cityOneData = JObject.Parse(cityOne);
                string descriptionOne = cityOneData.weather[0] != null ? cityOneData.weather[0].description : "the sun will rise from the east as usual.";            
                CurrentWeather cityOneWeather = new CurrentWeather()
                {
                    place = cityOneData.name,
                    currentTemp = cityOneData.main.temp,
                    maxTemp = cityOneData.main.temp_max,
                    minTemp = cityOneData.main.temp_min,
                    description = descriptionOne,
                    humidity = cityOneData.main.humidity,
                    pressure = cityOneData.main.pressure,
                    windSpeed = cityOneData.wind.speed,
                    windDirection = cityOneData.wind.deg,
                };
                cityOneData = new JavaScriptSerializer().Serialize(cityOneWeather);

                //City Two
                dynamic cityTwoData = JObject.Parse(cityTwo);
                string descriptionTwo = cityTwoData.weather[0] != null ? cityTwoData.weather[0].description : "The sun will rise from the east as usual.";
                CurrentWeather cityTwoWeather = new CurrentWeather()
                {
                    place = cityTwoData.name,
                    currentTemp = cityTwoData.main.temp,
                    maxTemp = cityTwoData.main.temp_max,
                    minTemp = cityTwoData.main.temp_min,
                    description = descriptionTwo,
                    humidity = cityTwoData.main.humidity,
                    pressure = cityTwoData.main.pressure,
                    windSpeed = cityTwoData.wind.speed,
                    windDirection = cityTwoData.wind.deg,
                };

                cityTwoData = new JavaScriptSerializer().Serialize(cityTwoWeather);

                //Return "same" object with current weather data
                RequestTwoCities theResult = new RequestTwoCities() {
                    cityone = cityOneData,
                    citytwo = cityTwoData,
                };

                return new ResponseData() { Status = 200, Data = JsonConvert.SerializeObject(theResult), Message = "" };
            }
            catch (Exception ex)
            {
                //TODO
                //Store ex for future debug purposes

                return new ResponseData() { Status = 400, Data = "", Message = "Please try again: "};
            }

        }
    }

}
