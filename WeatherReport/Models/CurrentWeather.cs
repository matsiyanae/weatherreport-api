﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherReport.Models
{
    public class CurrentWeather
    {
        public String place { get; set; }
        public Double currentTemp { get; set; }
        public Double maxTemp { get; set; }
        public Double minTemp { get; set; }
        public Double humidity { get; set; }
        public Double pressure { get; set; }
        public String windSpeed { get; set; }
        public String windDirection { get; set; }
        public String description { get; set; }
    }
}