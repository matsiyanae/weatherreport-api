﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherReport.Models
{
    public class RequestTwoCities
    {
        public String cityone { get; set; }
        public String citytwo { get; set; }
    }
}