﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherReport.Models
{
    /*
     Custom response object for all API requests
     Status: follow this 
     Data: is only available when no error has been generated in the response
     Message: Set this when user should view a message e.g When Status is 400, Message is the error related.
         */
    public class ResponseData
    {
        public Int64 Status { get; set; }
        public String Data { get; set; }
        public String Message { get; set; }
    }
}