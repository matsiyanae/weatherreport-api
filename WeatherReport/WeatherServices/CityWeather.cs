﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using WeatherReport.Classes;

namespace WeatherReport.WeatherServices
{
    public class CityWeather
    {
        public static async System.Threading.Tasks.Task<string> getCurrentWeather(string cityName)
        {

            string weather = "404";

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(Keys.OWMapi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string endpoint = "?units=metric&q=" + cityName + "&appid=" + Keys.WeatherAppId;
                    HttpResponseMessage response = await client.GetAsync(endpoint);
                    if (response.IsSuccessStatusCode)
                    {
                        weather = await response.Content.ReadAsStringAsync();
                    }
                }
                catch (Exception ex)
                {
                    //Ignore error for now
                    //Should be stored for future debug purposes
                }
            }

            return weather;

        }
    }
}